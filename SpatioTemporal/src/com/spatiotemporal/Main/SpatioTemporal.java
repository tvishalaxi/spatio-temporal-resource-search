package com.spatiotemporal.Main;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.spatiotemporal.Algorithms.Algorithms;
import com.spatiotemporal.Algorithms.Baseline;
import com.spatiotemporal.Algorithms.Probabilistic;
import com.spatiotemporal.Entity.Adjacency;
import com.spatiotemporal.Entity.Block;
import com.spatiotemporal.Entity.Graph;
import com.spatiotemporal.Entity.Node;
import com.spatiotemporal.Entity.Projection;
import com.spatiotemporal.utils.Utils;

public class SpatioTemporal {
	/*
	TODO: inserted by Raghavendra: [Mar 8, 2016:7:41:06 PM]
	All these global variables to be moved as a function variables
	*/
	public static List<Node> nodeList = new ArrayList<Node>();
	public static List<Block> blockList = new ArrayList<Block>();
	public static List<Projection> projectionList = new ArrayList<Projection>();
	public static TreeMap<Date,ArrayList<Long>> dateAndBlockDetails = 
			new TreeMap<Date,ArrayList<Long>>();
	public static TreeMap<Long,TreeMap<Long,Integer>> nodeDistMap = 
			new TreeMap<Long,TreeMap<Long,Integer>>(); 
	public static Map<Long,Block> blockDetails = new HashMap<Long,Block>();
	
	public static Map<Long,Node> nodeDetails = new HashMap<Long,Node>();
	public static Graph g = new Graph();
	public static ArrayList<Adjacency> adjacencyList = new ArrayList<Adjacency>();
	
	public static final int CONGESTION_LEVEL = 0;

	public static void main(String argus[]) throws IOException, ParseException
	{
		Utils.populateNode();
		Utils.populateBlock();
		Utils.populateProjection();
		Utils.populateAdjacency();
		//Should be handled in a switch case in order to 
		//switch between algos
		Algorithms chooseAlgo = new Baseline();
		
		/*Start probabilistic algorithm*/
		Utils.generatePreditiveModel_1(projectionList);
		Long inputBlockID=542301L;//326081L;//612312L;	//585092L;//
		
		Date input_timestamp=projectionList.get(9593).getTimestamp();
		SimpleDateFormat formatter = new SimpleDateFormat("y-M-d H:m:s.S");
		String inputTime = formatter.format(input_timestamp);
		input_timestamp = formatter.parse(inputTime);
		
		Probabilistic probAlgo = new Probabilistic();
		//System.out.println("**************************STARTING PROBABILISTIC ALGORITHM**************************");
		//System.out.println("BlockID="+inputBlockID+"  InputTime="+inputTime);
		//System.out.println("************************************************************************************");
		//probAlgo.runProbabilisticAlgorithm(inputBlockID,inputTime);
		for(int i=0;i<projectionList.size();i++){
			List<Node> nodesTraced = chooseAlgo.fetchRoute(projectionList.get(i).getBlockId(),projectionList.get(i).getTimestamp());
			/*for(Node node : nodesTraced){
				System.out.println(node.getNodeId());
			}*/
			//Probabilistic.total_time=0;
		}
		//List<Node> nodesTraced = chooseAlgo.fetchRoute(projectionList.get(0).getBlockId(),projectionList.get(0).getTimestamp());
		//List<Node> nodesTraced = chooseAlgo.fetchRoute(inputBlockID, input_timestamp);
		/*for(Node node : nodesTraced){
			System.out.println(node.getNodeId());
		}*/
		//System.out.println("Done!");
	}
}
